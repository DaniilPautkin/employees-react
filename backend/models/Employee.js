import mongoose from 'mongoose'

const Schema = mongoose.Schema

const EmployeeSchema = new Schema({
    fullName: {
        type: String
    },
    department: {
        type: String
    },
    note: {
        type: String
    }
})

mongoose.model('Employee', EmployeeSchema)