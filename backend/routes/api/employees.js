const express = require("express");
import '../../models/Employee'
const router = express.Router();
const mongoose = require('mongoose')
const Employee = mongoose.model('Employee')

router.get('/', (req, res) => {
    Employee.find().then(data => res.send(data))
})

router.get('/:id', (req, res) => {
    Employee.findById(req.params.id).then(data => res.send(data))
})

router.post('/add', (req, res) => {
    const employee = new Employee({
        fullName: req.body.fullName,
        department: req.body.department,
        note: req.body.note
    })
    return employee.save().then(data => res.send(data))
})

router.delete('/delete/:id', (req, res) => {
    Employee.findById(req.params.id).deleteOne().then(data => res.send(data))
})

router.post('/edit/:id', (req, res) => {
    Employee.updateOne({ _id: data.params.id }, {
        $set: {
            fullName: data.body.fullName,
            department: data.body.department,
            note: data.body.note
        }
    }).then(data => res.send(data))
})

module.exports = router