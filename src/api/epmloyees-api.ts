import { EmployeeType } from './../types/types';
import axios from 'axios'

const instance = axios.create({
    baseURL: 'http://localhost:5000/api/employees/'
})

export const employeesAPI = {
    getApiEmployees() {
        return instance.get<ResponseType<EmployeeResponseType>>('').then((res: any) => (res.data))
    },
    getApiEmployee(id: string | undefined) {
        return instance.get<ResponseType<EmployeeResponseType>>(`${id}`).then((res: any) => (res.data))
    },
    addApiEmployee(newEmployee: EmployeeType) {
        return instance.post('add', newEmployee)
    },
    updateApiEmployee(id: string | undefined, newEmployee: EmployeeType) {
        return instance.post(`edit/${id}`, newEmployee)
    },
    deleteApiEmployees(id: string | undefined) {
        return instance.delete(`delete/${id}`)
    }
}

type EmployeeResponseType = {
    id?: string
    fullName: string
    department: string
    notes: string
}

export type ResponseType<D = {}> = {
    data: D
    messages: Array<string>
}