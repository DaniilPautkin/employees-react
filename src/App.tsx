import 'antd/dist/antd.css'
import jwt_decode from 'jwt-decode'
import React from 'react'
import { Provider } from 'react-redux'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import { withSuspense } from './components/hoc/withSuspense'
import { Main } from './components/Styles/app-styles'
import setAuthToken from './components/utils/setAuthToken'
import { authActions, logoutUser } from './redux/auth-reducer'
import store from './redux/redux-store'

const EmployeeListContainer = React.lazy(() =>
    import('./components/Main/EmployeeListContainer')
)

const AboutPageContainer = React.lazy(() =>
    import('./components/Main/About/AboutPageContainer')
)

const LoginContainer = React.lazy(() =>
    import('./components/Auth/LoginContainer')
)

const RegisterContainer = React.lazy(() =>
    import('./components/Auth/RegisterContainer')
)

const SuspendedEmployeeList = withSuspense(EmployeeListContainer)
const SuspendedAboutPage = withSuspense(AboutPageContainer)
const SuspendedLogin = withSuspense(LoginContainer)
const SuspendedRegister = withSuspense(RegisterContainer)

if (localStorage.jwtToken) {
    const token = localStorage.jwtToken
    setAuthToken(token)

    const decoded = jwt_decode<any>(token)
    store.dispatch(authActions.setCurrentUser(decoded))

    const currentTime = Date.now() / 1000
    if (decoded.exp < currentTime) {
        logoutUser()
    }
}

const App: React.SFC = () => {
    return (
        <Provider store={store}>
            <BrowserRouter>
                <Main>
                    <Switch>
                        <Route
                            exact
                            path="/"
                            render={() => (
                                <div>
                                    <SuspendedEmployeeList />
                                </div>
                            )}
                        />
                        <Route
                            path="/about/:id"
                            render={() => <SuspendedAboutPage />}
                        />
                        <Route
                            exact
                            path="/auth/login"
                            render={() => <SuspendedLogin />}
                        />
                        <Route
                            exact
                            path="/auth/register"
                            render={() => <SuspendedRegister />}
                        />
                    </Switch>
                </Main>
            </BrowserRouter>
        </Provider>
    )
}

export default App
