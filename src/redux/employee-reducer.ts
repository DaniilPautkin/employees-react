import { Dispatch } from "react";
import { employeesAPI } from '../api/epmloyees-api';
import { EmployeeType } from './../types/types';
import { BasicThunkType, InferActionsType } from "./redux-store";

const InitialState = {
    isFetching: true,
    employee: { department: '', fullName: '', note: '' } as EmployeeType
}

const EmployeeReducer = (state = InitialState, action: ActionsType): InitialStateType => {
    switch (action.type) {
        case 'EMPLOYEES-REACT/EMPLOYEE/SET_EMPLOYEE':
            return {
                ...state, employee: action.employee
            }
        case 'EMPLOYEES-REACT/EMPLOYEE/TOGGLE_IS_FETCHING':
            return {
                ...state, isFetching: action.isFetching
            }
        default:
            return state
    }
}

export const actions = {
    setEmployee: (employee: EmployeeType) => ({ type: 'EMPLOYEES-REACT/EMPLOYEE/SET_EMPLOYEE', employee } as const),
    toggleIsFetching: (isFetching: boolean) => ({ type: 'EMPLOYEES-REACT/EMPLOYEE/TOGGLE_IS_FETCHING', isFetching } as const)
}

export const getEmployee = (EmployeeId: string | undefined): ThunkType => {
    return async (dispatch: Dispatch<ActionsType>) => {
        dispatch(actions.toggleIsFetching(true))
        let data = await employeesAPI.getApiEmployee(EmployeeId)
        dispatch(actions.setEmployee(data))
        dispatch(actions.toggleIsFetching(false))
    }
}

export const updateEmployee = (EmployeeId: string | undefined, EditedEmployeeProfile: EmployeeType): ThunkType => {
    return async (dispatch: Dispatch<ActionsType>) => {
        dispatch(actions.toggleIsFetching(true))
        await employeesAPI.updateApiEmployee(EmployeeId, EditedEmployeeProfile)
        dispatch(actions.setEmployee({ department: '', fullName: '', note: '' }))
        dispatch(actions.toggleIsFetching(false))
    }
}

type InitialStateType = typeof InitialState
type ActionsType = InferActionsType<typeof actions>
type ThunkType = BasicThunkType<ActionsType>

export default EmployeeReducer