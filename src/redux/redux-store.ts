import { combineReducers, createStore, applyMiddleware, Action } from 'redux'
import { compose } from 'redux'
import employeesReducer from './employees-reducer'
import thunkMiddleware, { ThunkAction } from 'redux-thunk'
import employeeReducer from './employee-reducer'
import authReducer from './auth-reducer'
import authErrorReducer from './auth-error-reducer'
import { reducer as formReducer } from 'redux-form'

let rootReducers = combineReducers({
    form: formReducer,
    auth: authReducer,
    authError: authErrorReducer,
    employees: employeesReducer,
    employeeProfile: employeeReducer
})

let store = createStore(
    rootReducers,
    compose(
        applyMiddleware(thunkMiddleware)
    )
)

//@ts-ignore
window.__store__ = store

export type AppStateType = ReturnType<typeof rootReducers>
export type InferActionsType<T> = T extends {
    [keys: string]: (...args: any[]) => infer U
}
    ? U
    : never
export type BasicThunkType<A extends Action, R = Promise<void>> = ThunkAction<
    R,
    AppStateType,
    unknown,
    A
>

export default store
