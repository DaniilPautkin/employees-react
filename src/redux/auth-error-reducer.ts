import { InferActionsType } from './redux-store';
const initialState = {
    errors: ''
}

const authErrorReducer = (state = initialState, action: ActionsType) => {
    switch (action.type) {
        case 'EMPLOYEES-REACT/USER/AUTH-ERROR/GET_ERRORS':
        default:
            return { ...state }
    }
}

export const authErrorActions = {
    getErrors: (payload: any) => ({ type: 'EMPLOYEES-REACT/USER/AUTH-ERROR/GET_ERRORS', payload } as const)
}

type ActionsType = InferActionsType<typeof authErrorActions>

export default authErrorReducer