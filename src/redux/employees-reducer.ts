import { Dispatch } from "react";
import { employeesAPI } from '../api/epmloyees-api';
import { EmployeeType } from './../types/types';
import { BasicThunkType, InferActionsType } from "./redux-store";

const InitialState = {
    isFetching: false,
    employees: [] as Array<EmployeeType>,
    searchFilterValue: '' as string,
    selectFilterValue: '' as string
}

const EmployeesReducer = (state = InitialState, action: ActionsType): InitialStateType => {
    switch (action.type) {
        case 'EMPLOYEES-REACT/EMPLOYEES/SET_EMPLOYEES':
            return {
                ...state, employees: action.employees
            }
        case 'EMPLOYEES-REACT/EMPLOYEES/TOGGLE_IS_FETCHING':
            return {
                ...state, isFetching: action.isFetching
            }
        case 'EMPLOYEES-REACT/EMPLOYEES/SET_SEARCH_FILTER_VALUES':
            return {
                ...state, searchFilterValue: action.searchFilterValue
            }
        case 'EMPLOYEES-REACT/EMPLOYEES/SET_SELECT_FILTER_VALUE':
            return {
                ...state, selectFilterValue: action.selectFilterValue
            }
        default:
            return state
    }
}

export const actions = {
    setEmployees: (employees: EmployeeType[]) => ({ type: 'EMPLOYEES-REACT/EMPLOYEES/SET_EMPLOYEES', employees } as const),
    toggleIsFetching: (isFetching: boolean) => ({ type: 'EMPLOYEES-REACT/EMPLOYEES/TOGGLE_IS_FETCHING', isFetching } as const),
    setSearchFilterValue: (searchFilterValue: string) => ({ type: 'EMPLOYEES-REACT/EMPLOYEES/SET_SEARCH_FILTER_VALUES', searchFilterValue } as const),
    setSelectFilterValue: (selectFilterValue: string) => ({ type: 'EMPLOYEES-REACT/EMPLOYEES/SET_SELECT_FILTER_VALUE', selectFilterValue } as const)

}

export const changeSearchFilterValue = (searchFilterValue: string) => {
    return async (dispatch: Dispatch<ActionsType>) => {
        dispatch(actions.toggleIsFetching(true))
        dispatch(actions.setSearchFilterValue(searchFilterValue))
        dispatch(actions.toggleIsFetching(false))
    }
}

export const changeSelectFilterValue = (selectFilterValue: string) => {
    return async (dispatch: Dispatch<ActionsType>) => {
        dispatch(actions.toggleIsFetching(true))
        dispatch(actions.setSelectFilterValue(selectFilterValue))
        dispatch(actions.toggleIsFetching(false))
    }
}

export const addEmployee = (newEmployee: EmployeeType): ThunkType => {
    return async (dispatch: Dispatch<ActionsType>) => {
        dispatch(actions.toggleIsFetching(true))
        await employeesAPI.addApiEmployee(newEmployee)
        dispatch(actions.toggleIsFetching(false))
    }
}

export const deleteEmployee = (EmployeeId: string | undefined): ThunkType => {
    return async (dispatch: Dispatch<ActionsType>) => {
        dispatch(actions.toggleIsFetching(true))
        await employeesAPI.deleteApiEmployees(EmployeeId)
        dispatch(actions.toggleIsFetching(false))
    }
}

export const getEmployees = (): ThunkType => {
    return async (dispatch: Dispatch<ActionsType>) => {
        dispatch(actions.toggleIsFetching(true))
        let data = await employeesAPI.getApiEmployees()
        dispatch(actions.setEmployees(data))
        dispatch(actions.toggleIsFetching(false))
    }
}

type InitialStateType = typeof InitialState
type ActionsType = InferActionsType<typeof actions>
type ThunkType = BasicThunkType<ActionsType>

export default EmployeesReducer