import React, { useEffect, useState } from 'react'
import { connect } from 'react-redux'
import { useHistory, useLocation } from 'react-router-dom'
import { getEmployee, updateEmployee } from '../../../redux/employee-reducer'
import { AppStateType } from '../../../redux/redux-store'
import { EmployeeType } from '../../../types/types'
import PreloaderContainer from '../../common/Preloader'
import AboutPage from './AboutPage'

const AboutPageContainer: React.FC<PropsType> = ({
    isFetching,
    employee,
    getEmployee,
    updateEmployee,
}) => {
    const [isEditMode, setEditMode] = useState(false)

    const location = useLocation()
    const history = useHistory()
    const userId = location.pathname.split('/').slice(-1)[0] // NOTE: to get userId from url in FC
    const editModeURL = location.pathname.split('/').slice(-2)[0]

    const [formFullName, setFullName] = useState(employee.fullName)
    const [formDepartment, setDepartment] = useState(employee.department)
    const [formNote, setNote] = useState(employee.note)

    const toggleEditMode = (e: React.MouseEvent<HTMLElement, MouseEvent>) => {
        setEditMode(!isEditMode)
    }

    const clearAllInputs = () => {
        setFullName('')
        setDepartment('')
        setNote('')
    }

    const changeFullName = (e: React.ChangeEvent<HTMLInputElement>) => {
        setFullName(e.target.value)
    }
    const changeDepartment = (e: React.ChangeEvent<HTMLInputElement>) => {
        setDepartment(e.target.value)
    }
    const changeNote = (e: React.ChangeEvent<HTMLInputElement>) => {
        setNote(e.target.value)
    }

    const submitChangeEmployeeProfile = () => {
        const newEmployeeProfile = {
            fullName: formFullName,
            department: formDepartment,
            note: formNote,
        }
        updateEmployee(userId, newEmployeeProfile)
        clearAllInputs()
        setEditMode(false)
        history.push('/')
    }

    const refuseChangeEmployeeProfile = () => {
        setEditMode(false)
    }

    useEffect(() => {
        if (!userId) {
            history.push('/')
        } else {
            getEmployee(userId)
            setFullName(employee.fullName)
            setDepartment(employee.department)
            setNote(employee.note)
            if (editModeURL === 'edit') {
                setEditMode(true)
            }
        }
    }, [employee.fullName, employee.department, employee.note])
    return (
        <div>
            {!!isFetching ? (
                <PreloaderContainer />
            ) : (
                <AboutPage
                    id={employee._id}
                    editModeURL={editModeURL}
                    changeFullName={changeFullName}
                    changeDepartment={changeDepartment}
                    changeNote={changeNote}
                    cancel={refuseChangeEmployeeProfile}
                    submit={submitChangeEmployeeProfile}
                    fullName={formFullName}
                    department={formDepartment}
                    note={formNote}
                    isEditMode={isEditMode}
                    toggleEditMode={toggleEditMode}
                />
            )}
        </div>
    )
}

const MapStateToProps = (state: AppStateType) => {
    return {
        isFetching: state.employeeProfile.isFetching,
        employee: state.employeeProfile.employee,
    }
}

type PropsType = MapStatePropsType & DispatchPropsType

type DispatchPropsType = {
    updateEmployee: (
        id: string,
        EditedEmployeeProfile: EmployeeType
    ) => void
    getEmployee: (id?: string) => void
}

type MapStatePropsType = ReturnType<typeof MapStateToProps>

export default connect(MapStateToProps, { updateEmployee, getEmployee })(
    AboutPageContainer
)
