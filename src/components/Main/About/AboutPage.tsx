import { CaretLeftOutlined } from '@ant-design/icons'
import { Button, Input } from 'antd'
import Text from 'antd/lib/typography/Text'
import React from 'react'
import { NavLink } from 'react-router-dom'
import {
    AboutEmployeeContainer,
    ButtonContainer,
    StyledAboutPage
} from '../../Styles/employee-styles'

const AboutPage: React.FC<PropsType> = ({
    id,
    changeFullName,
    changeDepartment,
    changeNote,
    cancel,
    submit,
    fullName,
    department,
    note,
    toggleEditMode,
    editModeURL,
}) => {
    return (
        <div>
            <StyledAboutPage>
                {editModeURL === 'edit' ? (
                    <>
                        {' '}
                        <Text code>Fullname: </Text>
                        <Input
                            maxLength={20}
                            allowClear
                            onChange={changeFullName}
                            value={fullName}
                            required
                        />
                        <Text code>Department: </Text>
                        <Input
                            maxLength={20}
                            allowClear
                            onChange={changeDepartment}
                            value={department}
                            required
                        />
                        <Text code>Note: </Text>
                        <Input
                            onChange={changeNote}
                            value={note}
                            maxLength={100}
                            allowClear
                            required
                        />
                        <ButtonContainer>
                            <Button onClick={cancel}>
                                <NavLink to={`/about/${id}`}>Cancel</NavLink>
                            </Button>

                            <Button onClick={submit} type="primary">
                                Save
                            </Button>
                        </ButtonContainer>
                    </>
                ) : (
                    <>
                        {' '}
                        <AboutEmployeeContainer>
                            <Text type="secondary">
                                Fullname: <Text>{fullName}</Text>
                            </Text>
                            <Text type="secondary">
                                Department: <Text>{department}</Text>
                            </Text>
                            <Text type="secondary">
                                Note: <Text>{note}</Text>
                            </Text>
                        </AboutEmployeeContainer>
                        <ButtonContainer>
                            <Button onClick={toggleEditMode} type="dashed">
                                <NavLink to="/">
                                    <CaretLeftOutlined /> Go Back
                                </NavLink>
                            </Button>
                            <Button onClick={toggleEditMode} type="primary">
                                <NavLink to={`/about/edit/${id}`}>Edit</NavLink>
                            </Button>
                        </ButtonContainer>
                    </>
                )}
            </StyledAboutPage>
        </div>
    )
}

type PropsType = MapPropsType & DispatchPropsType

type MapPropsType = {
    fullName: string
    department: string
    note: string
    isEditMode: boolean
    editModeURL: string
    id?: string
}

type DispatchPropsType = {
    changeFullName: (e: React.ChangeEvent<HTMLInputElement>) => void
    changeDepartment: (e: React.ChangeEvent<HTMLInputElement>) => void
    changeNote: (e: React.ChangeEvent<HTMLInputElement>) => void
    cancel: (e: React.MouseEvent<HTMLElement, MouseEvent>) => void
    submit: (e: React.MouseEvent<HTMLElement, MouseEvent>) => void
    toggleEditMode: (e: React.MouseEvent<HTMLElement, MouseEvent>) => void
}

export default AboutPage
