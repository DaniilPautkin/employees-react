import Text from 'antd/lib/typography/Text'
import React from 'react'
import { EmployeeType } from '../../types/types'
import {
    StyledEmployeeItem,
    StyledEmployeeList,
} from '../Styles/employee-styles'
import EmployeeItemContainer from './EmployeeItem/EmployeeItemContainer'

const EmployeeList: React.FC<PropsType> = ({
    deleteEmployee,
    employees,
    searchFilterValues,
    selectFilterValues,
}) => {
    const filterValues = () => {
        let filteredEmployees = []
        if (selectFilterValues === '' || !selectFilterValues) {
            // if default select search only by search input
            filteredEmployees = employees.filter((e: EmployeeType) =>
                e.fullName.toLowerCase().includes(searchFilterValues)
            )
            return (
                <div>
                    {filteredEmployees.map(
                        (employee: EmployeeType, index: number) => (
                            <EmployeeItemContainer
                                deleteEmployee={deleteEmployee}
                                id={employee._id}
                                key={++index} // to start from 1
                                employee={employee}
                                index={index}
                            />
                        )
                    )}
                </div>
            )
        } else {
            filteredEmployees = employees.filter(
                (e: EmployeeType) =>
                    e.fullName.toLowerCase().includes(searchFilterValues) &&
                    e.department === selectFilterValues
            )
            return (
                <div>
                    {filteredEmployees.map(
                        (employee: EmployeeType, index: number) => (
                            <EmployeeItemContainer
                                deleteEmployee={deleteEmployee}
                                id={employee._id}
                                key={++index} // to start from 1
                                employee={employee}
                                index={index}
                            />
                        )
                    )}
                </div>
            )
        }
    }

    return (
        <StyledEmployeeList>
            <hr />
            <StyledEmployeeItem>
                <Text type="secondary">#</Text>
                <Text type="secondary">name</Text>
                <Text type="secondary">department</Text>
                <Text type="secondary">edit</Text>
                <Text type="secondary">delete</Text>
            </StyledEmployeeItem>
            <hr />
            {filterValues()}
        </StyledEmployeeList>
    )
}

type PropsType = MapStatePropsType & DispatchPropsType

type DispatchPropsType = {
    deleteEmployee: (id?: string) => void
}

type MapStatePropsType = {
    employees: EmployeeType[]
    searchFilterValues: string
    selectFilterValues: string
}

export default EmployeeList
