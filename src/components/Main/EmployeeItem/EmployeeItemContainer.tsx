import React from 'react'
import { EmployeeType } from '../../../types/types'
import EmployeeItem from './EmployeeItem'

const EmployeeItemContainer: React.FC<PropsType> = ({
    deleteEmployee,
    id,
    employee,
    index,
}) => {
    return (
        <div>
            <EmployeeItem deleteEmployee={deleteEmployee} id={id} employee={employee} index={index} />
        </div>
    )
}
type PropsType = MapStatePropsType & DispatchPropsType

type DispatchPropsType = {
    deleteEmployee: (id?: string) => void
}

type MapStatePropsType = {
    id?: string
    employee: EmployeeType
    index: number
}

export default EmployeeItemContainer
