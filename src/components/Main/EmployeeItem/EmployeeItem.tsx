import { Button } from 'antd'
import Text from 'antd/lib/typography/Text'
import React, { useState } from 'react'
import { NavLink } from 'react-router-dom'
import { EmployeeType } from '../../../types/types'
import { StyledEmployeeItem } from '../../Styles/employee-styles'
import { trimString } from '../../utils/trimString'
import DeletePopUp from './DeletePopUp'

const EmployeeItem: React.FC<PropsType> = ({
    id,
    employee,
    index,
    deleteEmployee,
}) => {
    const [modalIsVisible, setModalIsVisible] = useState(false)

    const showModal = () => {
        setModalIsVisible(true)
    }

    const handleOk = (e: React.MouseEvent<HTMLElement, MouseEvent>) => {
        deleteEmployee(id)
        setModalIsVisible(false)
    }

    const handleCancel = (e: React.MouseEvent<HTMLElement, MouseEvent>) => {
        setModalIsVisible(false)
    }
    return (
        <div>
            <StyledEmployeeItem>
                <Text code>{index}</Text>
                <NavLink to={`/about/${id}`}>
                    {trimString(employee.fullName, 7)}
                </NavLink>
                <Text>{trimString(employee.department, 10)}</Text>
                <NavLink to={`/about/edit/${id}`}>Edit</NavLink>
                <Button type="link" onClick={showModal}>
                    Delete
                </Button>
            </StyledEmployeeItem>
            <DeletePopUp
                modalIsVisible={modalIsVisible}
                id={id}
                handleOk={handleOk}
                handleCancel={handleCancel}
                fullName={employee.fullName}
            />
        </div>
    )
}

type PropsType = MapStatePropsType & DispatchPropsType

type DispatchPropsType = {
    deleteEmployee: (id?: string) => void
}

type MapStatePropsType = {
    id?: string
    employee: EmployeeType
    index: number
}

export default EmployeeItem
