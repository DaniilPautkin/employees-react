import { Modal } from 'antd'
import Text from 'antd/lib/typography/Text'
import React from 'react'

const DeletePopUp: React.FC<PropsType> = ({id,
    modalIsVisible,
    handleOk,
handleCancel,
fullName}) =>
    {
        return (
            <div>
                <Modal
                    title="Warning"
                    visible={modalIsVisible}
                    onOk={handleOk}
                    onCancel={handleCancel}
                >
                    <Text type="secondary">
                        Are you sure you want to delete{' '}
                    </Text>{' '}
                    {fullName}
                    <Text type="secondary"> ? </Text>
                    {/* <Button onClick={handleCancel}>Cancel</Button> */}
                    {/* <Button onClick={handleOk}>Ok</Button> */}
                </Modal>
            </div>
        )
    }

type PropsType = MapStateType & DispatchType

type MapStateType = {
    id?: string
    modalIsVisible: boolean
    fullName: string
}

type DispatchType = {
    handleOk: (e: React.MouseEvent<HTMLElement, MouseEvent>) => void
    handleCancel: (e: React.MouseEvent<HTMLElement, MouseEvent>) => void
}

export default DeletePopUp
