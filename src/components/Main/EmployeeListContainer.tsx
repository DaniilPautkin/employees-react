import { Empty } from 'antd'
import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { deleteEmployee, getEmployees } from '../../redux/employees-reducer'
import { AppStateType } from '../../redux/redux-store'
import { EmployeeType } from '../../types/types'
import BarContainer from '../Bar/BarContainer'
import PreloaderContainer from '../common/Preloader'
import { withAuthRedirect } from '../hoc/withAuthRedirect'
import EmployeeList from './EmployeeList'

const EmployeeListContainer: React.FC<PropsType> = ({
    deleteEmployee,
    isFetching,
    employees,
    getEmployees,
    searchFilterValues,
    selectFilterValues,
}) => {
    useEffect(() => {
        getEmployees()
    }, [])

    const onDeleteEmployee = (id?: string) => {
        deleteEmployee(id)
    }

    return (
        <div>
            {!!isFetching ? (
                <PreloaderContainer />
            ) : (
                <>
                    <BarContainer />
                    {!!employees ? (
                        <>
                            <EmployeeList
                            deleteEmployee={onDeleteEmployee}
                                searchFilterValues={searchFilterValues}
                                selectFilterValues={selectFilterValues}
                                employees={employees}
                            />
                        </>
                    ) : (
                        <Empty />
                    )}
                    {/* <DeletePopUp showModal={showModal} modalIsVisible={modalIsVisible} /> */}
                </>
            )}
        </div>
    )
}

const MapStateToProps = (state: AppStateType) => {
    return {
        isFetching: state.employees.isFetching,
        employees: state.employees.employees,
        searchFilterValues: state.employees.searchFilterValue,
        selectFilterValues: state.employees.selectFilterValue,
    }
}

type PropsType = MapPropsType & DispatchPropsType

type DispatchPropsType = {
    deleteEmployee: (id?: string) => void
    getEmployees: () => void
}

type MapPropsType = {
    isFetching: boolean
    employees: EmployeeType[]
    searchFilterValues: string
    selectFilterValues: string
}

export default compose<React.ComponentType>(
    connect(MapStateToProps, { getEmployees, deleteEmployee }),
    withAuthRedirect
)(EmployeeListContainer)
