import React, { useState } from 'react'
import { connect } from 'react-redux'
import { addEmployee } from '../../redux/employees-reducer'
import { AppStateType } from '../../redux/redux-store'
import { logoutUser } from '../../redux/auth-reducer'
import { EmployeeType } from '../../types/types'
import AddEmployeeModalContainer from './AddEmployeeModal'
import Bar from './Bar'
import { Button } from 'antd'
import Text from 'antd/lib/typography/Text'
import { Header } from '../Styles/header-styles'

const BarContainer: React.FC<PropsType> = ({
    user,
    employees,
    addEmployee,
    logoutUser,
}) => {
    const [modalIsVisible, setModalIsVisible] = useState(false)
    const [fullName, setFullName] = useState('')
    const [department, setDepartment] = useState('')
    const [note, setNote] = useState('')

    const changeFullName = (e: React.ChangeEvent<HTMLInputElement>) => {
        setFullName(e.target.value)
    }
    const changeDepartment = (value: string) => {
        setDepartment(value)
    }
    const changeNewDepartment = (e: React.ChangeEvent<HTMLInputElement>) => {
        setDepartment(e.target.value)
    }
    const changeNote = (e: React.ChangeEvent<HTMLInputElement>) => {
        setNote(e.target.value)
    }

    const clearAllInputs = () => {
        setFullName('')
        setDepartment('')
        setNote('')
    }
    const showModal = () => {
        setModalIsVisible(true)
    }
    const handleOk = (e: React.MouseEvent<HTMLElement, MouseEvent>) => {
        const newEmployee = {
            fullName: fullName,
            department: department,
            note: note,
        }
        addEmployee(newEmployee)
        setModalIsVisible(false)
    }
    const handleCancel = (e: React.MouseEvent<HTMLElement, MouseEvent>) => {
        clearAllInputs()
        setModalIsVisible(false)
    }

    const onLogoutUser = () => {
        logoutUser()
    }

    let departmentsArr: string[] = []
    employees.forEach((e) => departmentsArr.push(e.department)) // pick department from employee[]
    const selectDepartmentValues = departmentsArr.filter(
        (item: string, pos: number) => {
            // remove repeated values
            return departmentsArr.indexOf(item) === pos
        }
    )

    return (
        <>
            <Header>
                {user.name && <Text code>{user.name}</Text>}
                <Button type="link" onClick={onLogoutUser}>
                    Logout
                </Button>
            </Header>
            <Bar showModal={showModal} />
            <AddEmployeeModalContainer
                department={department}
                changeNewDepartment={changeNewDepartment}
                selectDepartmentValues={selectDepartmentValues}
                modalIsVisible={modalIsVisible}
                handleOk={handleOk}
                handleCancel={handleCancel}
                changeFullName={changeFullName}
                fullName={fullName}
                changeDepartment={changeDepartment}
                changeNote={changeNote}
                note={note}
            />
        </>
    )
}

const MapStateToProps = (state: AppStateType) => {
    return {
        user: state.auth.user,
        employees: state.employees.employees,
    }
}

type PropsType = DispatchPropsType & MapStateType

type MapStateType = ReturnType<typeof MapStateToProps>

type DispatchPropsType = {
    addEmployee: (newEmployee: EmployeeType) => void
    logoutUser: () => void
}

export default connect(MapStateToProps, { addEmployee, logoutUser })(
    BarContainer
)
