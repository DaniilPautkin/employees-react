import React from 'react'
import { connect } from 'react-redux'
import { changeSelectFilterValue } from '../../../redux/employees-reducer'
import { AppStateType } from '../../../redux/redux-store'
import { EmployeeType } from '../../../types/types'
import PreloaderContainer from '../../common/Preloader'
import SelectInput from './SelectInput'

const SelectInputContainer: React.FC<PropsType> = ({
    isFetching,
    employees,
    changeSelectFilterValue,
}) => {
    let departmentsArr: string[] = []
    employees.forEach((e) => departmentsArr.push(e.department)) // pick department from employee[]

    const filterValues = departmentsArr.filter((item: string, pos: number) => {
        // remove repeated values
        return departmentsArr.indexOf(item) === pos
    })

    return (
        <div>
            {!!isFetching ? (
                <PreloaderContainer />
            ) : (
                <>
                    <SelectInput
                        changeSelectFilterValue={changeSelectFilterValue}
                        filterValues={filterValues}
                    />
                </>
            )}
        </div>
    )
}

const MapStateToProps = (state: AppStateType) => {
    return {
        isFetching: state.employees.isFetching,
        employees: state.employees.employees,
    }
}

type PropsType = MapPropsType & DispatchPropsType

type MapPropsType = {
    isFetching: boolean
    employees: EmployeeType[]
}

type DispatchPropsType = {
    changeSelectFilterValue: (filterValue: string) => void
}

export default connect(MapStateToProps, { changeSelectFilterValue })(
    SelectInputContainer
)
