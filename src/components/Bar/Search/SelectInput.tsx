import { Select } from 'antd'
import React from 'react'

const SelectInput: React.FC<PropsType> = ({
    filterValues,
    changeSelectFilterValue,
}) => {
    const ChangeFilterValue = (filterValue: string) => {
        changeSelectFilterValue(filterValue)
    }
    return (
        <div>
            <Select
                onChange={ChangeFilterValue}
                defaultValue=""
                style={{ width: 120 }}
                allowClear
            >
                {filterValues.map((option, index) => (
                    <Select.Option key={index} value={option}>
                        {!!option === true && option}
                    </Select.Option>
                ))}
            </Select>
        </div>
    )
}

type PropsType = MapStatePropsType & DispatchPropsType

type MapStatePropsType = {
    filterValues: string[]
}

type DispatchPropsType = {
    changeSelectFilterValue: (selectFilterValue: string) => void
}

export default SelectInput
