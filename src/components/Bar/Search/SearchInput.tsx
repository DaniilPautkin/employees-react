import { Input } from 'antd'
import React, { useState } from 'react'
import { connect } from 'react-redux'
import { changeSearchFilterValue } from '../../../redux/employees-reducer'
import { AppStateType } from '../../../redux/redux-store'

const SearchInput: React.FC<PropsType> = ({
    employees,
    changeSearchFilterValue,
}) => {
    const [serachFilterValues, setSearchFilter] = useState('')
    const onChangeSearchFilterValues = (
        e: React.ChangeEvent<HTMLInputElement>
    ) => {
        setSearchFilter(e.target.value) // change Input value
        changeSearchFilterValue(e.target.value.toLowerCase()) // change redux value
    }
    return (
        <div>
            <Input
                placeholder="Search by name..."
                value={serachFilterValues}
                onChange={onChangeSearchFilterValues}
            />
        </div>
    )
}

type PropsType = MapPropsType & DispatchPropsType

type MapPropsType = ReturnType<typeof MapStateToProps>

type DispatchPropsType = {
    changeSearchFilterValue: (searchFilterValues: string) => void
}

const MapStateToProps = (state: AppStateType) => {
    return {
        employees: state.employees.employees,
    }
}

export default connect(MapStateToProps, { changeSearchFilterValue })(
    SearchInput
)
