import { Button } from 'antd'
import React from 'react'
import { StyledBar } from '../Styles/bar-styles'
import SearchInput from './Search/SearchInput'
import SelectInputContainer from './Search/SelectInputContainer'

const Bar: React.FC<PropsType> = ({ showModal }) => {
    return (
        <StyledBar>
            <SearchInput />
            <SelectInputContainer />
            <Button onClick={showModal} type="primary">
                Add
            </Button>
        </StyledBar>
    )
}

type PropsType = DispatchPropsType

type DispatchPropsType = {
    showModal: () => void
}

export default Bar
