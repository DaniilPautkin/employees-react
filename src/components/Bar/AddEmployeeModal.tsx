import { Input, Select, Button } from 'antd'
import Text from 'antd/lib/typography/Text'
import React, { useState } from 'react'
import {
    AddEmployeeModal,
    DepartmentSelectContainer,
    AddEmployeeDepartmentContainer,
} from '../Styles/employee-styles'

const AddEmployeeModalContainer: React.FC<PropsType> = ({
    department,
    changeNewDepartment,
    selectDepartmentValues,
    modalIsVisible,
    handleOk,
    handleCancel,
    changeFullName,
    fullName,
    changeDepartment,
    changeNote,
    note,
}) => {
    const [isNewDepartment, setIsNewDepartment] = useState(false)
    const changeIsNewDepartment = (
        e: React.MouseEvent<HTMLElement, MouseEvent>
    ) => {
        setIsNewDepartment(!isNewDepartment)
    }
    return (
        <AddEmployeeModal
            title="Add employee"
            visible={modalIsVisible}
            onOk={handleOk}
            onCancel={handleCancel}
        >
            <div>
                <Text>Fullname:</Text>
                <Input
                    maxLength={20}
                    onChange={changeFullName}
                    value={fullName}
                />
            </div>
            <DepartmentSelectContainer>
                <Text>Department:</Text>
                {!!isNewDepartment ? (
                    <>
                        <AddEmployeeDepartmentContainer>
                            <Input
                                maxLength={100}
                                onChange={changeNewDepartment}
                                value={department}
                            />
                            <Button
                                onClick={changeIsNewDepartment}
                                type="default"
                            >
                                Select existing department
                            </Button>
                        </AddEmployeeDepartmentContainer>
                    </>
                ) : (
                    <>
                        {' '}
                        <AddEmployeeDepartmentContainer>
                            <Select
                                onChange={(value) =>
                                    changeDepartment(value.toString())
                                }
                            >
                                {!!selectDepartmentValues === true &&
                                    selectDepartmentValues.map(
                                        (option, index) => (
                                            <Select.Option
                                                key={index}
                                                value={option}
                                            >
                                                {!!option === true && option}
                                            </Select.Option>
                                        )
                                    )}
                            </Select>
                            <Button
                                onClick={changeIsNewDepartment}
                                type="default"
                            >
                                Add new department
                            </Button>
                        </AddEmployeeDepartmentContainer>
                    </>
                )}
            </DepartmentSelectContainer>
            <div>
                <Text>Note:</Text>
                <Input maxLength={100} onChange={changeNote} value={note} />
            </div>
        </AddEmployeeModal>
    )
}

type PropsType = MapStatePropsType & DispatchPropsType

type MapStatePropsType = {
    selectDepartmentValues: string[]
    fullName: string
    note: string
    modalIsVisible: boolean
    department: string
}

type DispatchPropsType = {
    changeNewDepartment: (e: React.ChangeEvent<HTMLInputElement>) => void
    handleOk: (e: React.MouseEvent<HTMLElement, MouseEvent>) => void
    handleCancel: (e: React.MouseEvent<HTMLElement, MouseEvent>) => void
    changeFullName: (e: React.ChangeEvent<HTMLInputElement>) => void
    changeDepartment: (value: string) => void
    changeNote: (e: React.ChangeEvent<HTMLInputElement>) => void
}

export default AddEmployeeModalContainer
