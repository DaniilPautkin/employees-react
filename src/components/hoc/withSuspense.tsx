import React, { Suspense } from 'react'
import PreloaderContainer from '../common/Preloader'

export function withSuspense<WCP>(WrappedComponent: React.ComponentType<WCP>) {
    return (props: WCP) => {
        return (
            <Suspense fallback={<PreloaderContainer />}>
                <WrappedComponent {...props} />
            </Suspense>
        )
    }
}
