export const removeRepeatedValues = (repeatedValuesArr: any, pickString: string) => {

    let finalStringArr: string[] = []

    repeatedValuesArr.forEach((a: any) => finalStringArr.push(a.pickString))

    const filterValues = finalStringArr.filter((item: string, pos: number) => {
        // remove repeated values
        return finalStringArr.indexOf(item) === pos
    })
}