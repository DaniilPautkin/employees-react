export const trimString = (value: string | null, stringLen: number) => {
    let trimmedString =
        value && value.length > stringLen
            ? value.substring(0, stringLen - 3) + '...'
            : value
    return trimmedString
}