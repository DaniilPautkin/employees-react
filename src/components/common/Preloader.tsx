import React from 'react'
import { StyledSpin } from '../Styles/app-styles'

const PreloaderContainer = () => {
    return (
        <div>
            <StyledSpin />
        </div>
    )
}

export default PreloaderContainer
