import React from 'react'
import { connect } from 'react-redux'
import { Redirect } from 'react-router-dom'
import { loginUser } from '../../redux/auth-reducer'
import { AppStateType } from '../../redux/redux-store'
import PreloaderContainer from '../common/Preloader'
import Login from './Login'

const LoginContainer: React.FC<PropsType> = ({
    isAuthenticated,
    isFetching,
    loginUser,
}) => {
    return (
        <div>
            {isAuthenticated ? (
                <Redirect to="/" />
            ) : (
                <>
                    {' '}
                    {!!isFetching ? (
                        <PreloaderContainer />
                    ) : (
                        <Login
                            isAuthenticated={isAuthenticated}
                            loginUser={loginUser}
                        />
                    )}
                </>
            )}
        </div>
    )
}

type PropsType = MapStatePropsType & DispatchType

type DispatchType = {
    loginUser: (email: string, password: string) => void
}

type MapStatePropsType = ReturnType<typeof MapStateToProps>

const MapStateToProps = (state: AppStateType) => {
    return {
        isAuthenticated: state.auth.isAuthenticated,
        isFetching: state.auth.isFetching,
    }
}

export default connect(MapStateToProps, { loginUser })(LoginContainer)
