import styled from 'styled-components'
import { Spin } from 'antd'

export const Main = styled.div`
    width: 40%;
    padding: 10px;
    border-radius: 10px;
    margin: auto;
    background: #f1f1f1;
    @media (max-width: 900px) {
        width: 90%;
    }
`

export const StyledSpin = styled(Spin)`
    margin: 0 auto !important;
    align-self: center !important;
`
