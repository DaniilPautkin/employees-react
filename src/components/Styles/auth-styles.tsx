import styled from 'styled-components'

export const StyledAuthContainer = styled.div`
    width: 40%;
    margin: 0 auto;
    display: flex;
    justify-content: center;
    flex-direction: column;
    /* .ant-space-item {
        margin-top: 10px;
        margin-bottom: 0px !important
    } */
    Text {
        margin: 0px !important;
    }
    Input {
        margin: 10px 0px 10px 0px;
    }
    Button {
        align-self: center;
        width: 50%;
    }
`

export const AuthButtonsContainer = styled.div`
    display: flex;
    justify-content: space-between;

    Button {
        width: 50%;
    }
`
