import styled from 'styled-components'

export const Header = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
    margin: 10px 0px 10px 0px;
`
