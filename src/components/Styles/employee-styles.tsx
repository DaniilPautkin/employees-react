import styled from 'styled-components'
import Modal from 'antd/lib/modal/Modal'

export const StyledEmployeeList = styled.div`
    display: flex;
    flex-direction: column;
`

export const StyledEmployeeItem = styled.div`
    display: grid;
    grid-template-columns: 1fr 4fr 4fr 2fr 2fr;
    align-items: center;
    border-top: 1px solid #d3d3d3;
    &:not(:first-child) {
        border-top: none;
    }
    text-align: center;
`

export const StyledAboutPage = styled.div`
    display: flex;
    flex-direction: column;
    padding: 10px;
    justify-content: flex-start;
    Text {
        margin-top: 60px;
    }
`

export const AboutEmployeeContainer = styled.div`
    display: grid;
`

export const ButtonContainer = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    margin: 10px 0px 10px 0px;
    Button {
        width: 30%;
    }
`

export const AddEmployeeModal = styled(Modal)`
    display: flex;
    flex-direction: column;
    .ant-select-selector {
        width: 100pt !important;
    }
`

export const AddEmployeeDepartmentContainer = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    width: 100%;
    Select {
        width: 500px;
    }
    Input {
        width: 50%;
    }
`

export const DepartmentSelectContainer = styled.div`
    display: flex;
    flex-direction: column;
`
